FROM python:3.7-alpine
 
ADD . /app
WORKDIR /app
 
RUN apk update && apk add bash
 
RUN apk update --no-cache \
# psycopg2 dependencies
&& apk add libxml2-dev libxslt-dev \ 
&& apk add --no-cache --virtual build-deps gcc python3-dev musl-dev \
&& apk add --no-cache --virtual .build-deps \
gcc \
musl-dev \
&& apk del .build-deps \
# Pillow dependencies
&& apk add --no-cache jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev \
# CFFI dependencies
&& apk add --no-cache libffi-dev py-cffi\
# libsass dependencies
&& apk add build-base \
&& apk add python3-dev
 
RUN pip install -r requirements.txt
RUN python manage.py migrate
 
EXPOSE 8000
 
CMD python manage.py runserver 0.0.0.0:8000